<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReservationRequest;
use App\Reservation;
use App\Unit;
use Carbon\CarbonPeriod;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ReservationCtrl extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(Reservation::with("unit")->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReservationRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( ReservationRequest $request ) {
        $data = $request->only(["unit_id", "date_from", "date_to", "first_name", "last_name", "email", "total_price"]);
        $isValidDateRange = self::isValidDateRange($data["unit_id"], $data["date_from"], $data["date_to"]);
        if(!$isValidDateRange) {
            throw new HttpResponseException(response()->json( ["date" => ["Date for this unit has been blocked"]], 422 ));
        }
        $reservation = Reservation::create($data);
        return response()->json(["status" => "success", "message" => "Reservation added successfully"]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        $reservation = Reservation::with('unit')->where("id", $id)->first();
//        $reservation->blocked_dates = self::getBlockedDatesList(data_get($reservation, "unit.id"));
        return response()->json($reservation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( ReservationRequest $request, $id ) {
        $data = $request->only(["unit_id", "date_from", "date_to", "first_name", "last_name", "email", "total_price"]);
        $reservation = Reservation::find($id);
        $isValidDateRange = self::isValidDateRange($data["unit_id"], $data["date_from"], $data["date_to"], $reservation);
        if(!$isValidDateRange) {
            throw new HttpResponseException(response()->json( ["date" => ["Date for this unit has been blocked"]], 422 ));
        }
        $reservation->update($data);
        return response()->json(["status" => "success", "message" => "Reservation updated successfully"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        $reservation = Reservation::where("id", $id)->first();
        if(!$reservation) {
            return response()->json(["status" => "fail", "message" => "Reservation with id not found"]);
        }
        $reservation->delete();
        return response()->json(["status" => "success", "message" => "Reservation deleted successfully"]);
    }


    private function isValidDateRange($unitId, $dateFrom, $dateTo, $reservation = "") {
        $datesToExclude = [];
        if($reservation) {
            $period = CarbonPeriod::create($reservation->date_from, $reservation->date_to)->toArray();
            foreach ($period as $date) {
                $datesToExclude[] = $date->format("Y-m-d");
            }
        }
        $unit = Unit::find($unitId);
        if(!empty($unit->blocked_dates)) {
            $period = CarbonPeriod::create($dateFrom, $dateTo)->toArray();
            foreach ($period as $date) {
                $dateFormat = $date->format("Y-m-d");
                if(in_array($dateFormat, $unit->blocked_dates) && (!in_array($dateFormat, $datesToExclude))) {
                    return false;
                }
            }
        }
        return true;
    }
}
