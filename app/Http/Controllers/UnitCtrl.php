<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest;
use App\Unit;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Ramsey\Uuid\Uuid;

class UnitCtrl extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return response()->json( Unit::with('reservations')->get() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UnitRequest $request
     *
     * @return Response
     */
    public function store( UnitRequest $request ) {
        $data = $request->only(["name", "max_persons", "price_per_night"]);
        $unit = Unit::create($data);
        return response()->json(["status" => "success", "message" => "Unit added successfully"]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show( $id ) {
        return response()->json(Unit::with('reservations')->where("id", $id)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UnitRequest $request
     * @param Uuid $id
     *
     * @return Response
     */
    public function update( UnitRequest $request, $id ) {
        $unit = Unit::where("id", $id)->first();
        if(!$unit) {
            return response()->json(["status" => "fails", "message" => "Unit with id not found"]);
        }
        $data = $request->only(["name", "max_persons", "price_per_night"]);
        $unit->update($data);
        return response()->json(["status" => "success", "message" => "Unit updated successfully"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy( $id ) {
        $unit = Unit::with("reservations")->where("id", $id)->first();
        if(!$unit) {
            return response()->json(["status" => "fail", "message" => "Unit with id not found"]);
        }
        if($unit->reservations->count()) {
            throw new HttpResponseException(response()->json( ["status" => "fail", "message" => "Unit can't be deleted because there are open reservations"], 422 ));
        }
        $unit->delete();
        return response()->json(["status" => "success", "message" => "Unit deleted successfully"]);
    }

}
