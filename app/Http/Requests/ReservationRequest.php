<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "unit_id" => "required|exists:units,id",
            "date_from" => "required|date_format:Y-m-d|before:date_to",
            "date_to" => "required|date_format:Y-m-d|after:date_from",
            "first_name" => "required|max:255",
            "last_name" => "required|max:255",
            "email" => "required|max:255|email",
            "total_price" => "required"
        ];
    }

    protected function failedValidation( Validator $validator ) {
        throw new HttpResponseException( response()->json( $validator->errors(), 422 ) );
    }
}
