<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UnitRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "name"            => "required|max:255",
            "max_persons"     => "required|integer|gt:0",
            "price_per_night" => "required|numeric|gt:0"
        ];
    }

    protected function failedValidation( Validator $validator ) {
        throw new HttpResponseException( response()->json( $validator->errors(), 422 ) );
    }
}
