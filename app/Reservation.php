<?php

namespace App;


use Carbon\CarbonPeriod;

class Reservation extends UuidModel {
    protected $fillable = [
        "unit_id",
        "date_from",
        "date_to",
        "total_price",
        "first_name",
        "last_name",
        "email"
    ];

    protected $appends = [
        "reservation_dates"
    ];

    function unit() {
        return $this->belongsTo(Unit::class);
    }

    function getReservationDatesAttribute() {
        if($this->date_from && $this->date_to) {
            $period = CarbonPeriod::create( $this->date_from, $this->date_to )->toArray();
            $reservationDates = [];
            foreach ( $period as $date ) {
                $dateFormat = $date->format( "Y-m-d" );
                $reservationDates[] = $dateFormat;
            }
            return $reservationDates;
        }
        return [];
    }
}
