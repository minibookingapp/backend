<?php

namespace App;


use Carbon\CarbonPeriod;

class Unit extends UuidModel {
    protected $fillable = [
        "name",
        "max_persons",
        "price_per_night"
    ];

    protected $appends = [
        "blocked_dates"
    ];

    function reservations() {
        return $this->hasMany(Reservation::class);
    }

    function getBlockedDatesAttribute() {
        $reservations = Reservation::where("unit_id", $this->id)->get();
        if(!empty($reservations)) {
            $blockDateList = [];
            foreach ($reservations as $reservation) {
                $period = CarbonPeriod::create($reservation->date_from, $reservation->date_to)->toArray();
                foreach ($period as $date) {
                    $dateFormat = $date->format("Y-m-d");
                    if(!in_array($dateFormat, $blockDateList)) {
                        $blockDateList[] = $dateFormat;
                    }
                }
            }
            return $blockDateList;
        }
        return [];
    }
}
