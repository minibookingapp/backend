<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class UuidModel extends Model {

    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating( function ( Model $model ) {
            $model->setAttribute( $model->getKeyName(), Uuid::uuid4() );
        } );
    }

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];
}
